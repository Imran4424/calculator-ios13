//
//  CalculatorLogic.swift
//  Calculator
//
//  Created by Imran Hossain on 26/9/21.
//  Copyright © 2021 London App Brewery. All rights reserved.
//

import Foundation

struct CalculatorLogic {
    
    private var number: Double?
    private var intermediateCalculation: (n1: Double, calcMethod: String)?
    
    mutating func setNumber(_ number: Double) {
        self.number = number
    }
    
    mutating func calculate(symbol: String) -> Double? {
        guard let num = number else {
            fatalError("number is nil")
        }
        
        if symbol == "+/-" {
            return num * -1
        } else if symbol == "AC" {
            return 0
        } else if symbol == "%" {
            return num * 0.01
        } else if symbol == "=" {
            return performTwoNumCalculation(n2: num)
        } else {
            intermediateCalculation = (n1: num, calcMethod: symbol)
        }
        
        return nil
    }
    
    private func performTwoNumCalculation(n2: Double) -> Double {
        guard let operand = intermediateCalculation?.n1 else {
            fatalError("first operand is nil")
        }
        
        guard let symbol = intermediateCalculation?.calcMethod else {
            fatalError("operator is nil")
        }
        
        if symbol == "+" {
            return operand + n2
        } else if symbol == "-" {
            return operand - n2
        } else if symbol == "×" {
            return operand * n2
        } else if symbol == "÷" {
            return operand / n2
        } else {
            fatalError("The operation passed in does not match any of the cases")
        }
    }
}
